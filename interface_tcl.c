#ifndef INTERFACE
#define INTERFACE

#include <stdio.h>
#include <stdlib.h>
#include <tcl.h>
#include <tk.h>
#include "constant.h"

void Tk_Main(int nbarg, char * listarg[ ], Tcl_AppInitProc * Tcl_AppInit);

#endif
