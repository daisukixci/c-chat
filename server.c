#include "server.h"

static pthread_mutex_t mutex;
static char shared_buffer[SIZE_MAXI+20];
int shared_buffer_update;
int writer_number;
char writers[20][NICKNAME_MAXI];

void server(struct option_s *options_p){
	int server_continue = 1;
	char *host = malloc(sizeof(char)*SIZE_HOST);
	int port, i, j;
	struct sockaddr_in socket_in = {0};
	socklen_t sock_len;
	int sock = socket( AF_INET, SOCK_STREAM, 0);
	int c_sock, *new_sock;
	struct sockaddr_in client_addr = {0};
	int client_addr_size = sizeof client_addr;
	pthread_t *client_thread = malloc(sizeof(pthread_t));
	writer_number = 0;
	for(i = 0; i < 20; i++){
		for(j = 0; j < NICKNAME_MAXI; j++)
			writers[i][j] = '\0';
	}
	if(sock == INVALID_SOCKET){
		perror("socket()");
		exit(errno);
	}

	show_ip();
	port = choose_port();
	inet_aton(host, &socket_in.sin_addr);
	socket_in.sin_port = htons(port);
	socket_in.sin_family = AF_INET;
	sock_len = sizeof socket_in;

	if (bind(sock,(struct sockaddr*) &socket_in, sock_len) == -1) {
		perror("bind()");
		exit(EXIT_FAILURE);
	}
	if(listen(sock, 10) == -1){
		perror("listen()");
		exit(EXIT_FAILURE);
	}

	fprintf(stdout, "PORT: %d\n", port);

	pthread_mutex_init(&mutex, NULL);

	while(c_sock = accept(sock, (struct sockaddr *)&client_addr, &client_addr_size)){
		new_sock = malloc(1);
		*new_sock = c_sock;
		if(new_sock < 0){
			perror("accept()");
			exit(EXIT_FAILURE);
		}
		else{
			if(pthread_create(client_thread, NULL, distribution_work, (void *)new_sock)){
				perror("pthread_create()");
				exit(EXIT_FAILURE);
			}
		}
	}
	free(host);
	free(client_thread);
	free(new_sock);
}

void * distribution_work(void * point){
	char buffer[SIZE_MAXI] = {'\0'};
	char *nickname;
	char writer[NICKNAME_MAXI];
	int c_sock = *(int*)point;
	int i = 0;
	if(recv(c_sock, buffer, sizeof buffer, 0) < 0 ){
		perror("recv()");
		exit(EXIT_FAILURE);
	}
	fprintf(stderr, "[DEBUG] buffer: %s\n", buffer);
	if(buffer[0] == 'W'){
		writer_number ++;
		nickname = catch_nickname(buffer);
		strcpy(writer, nickname);
		if(test_nickname(nickname) == -1){
			send_connexion_response(c_sock, NO);
			close(c_sock);
		}
		else{
			if(i = check_writers() != -1){
				pthread_mutex_lock(&mutex);
				add_writer(i, writers, writer);
				pthread_mutex_unlock(&mutex);
			}
			else{
				send_connexion_response(c_sock, NO);
				close(c_sock);
				fprintf(stderr, "[DEBUG] The server is full\n");
			}

			fprintf(stderr, "[DEBUG] WRITER CONNEXION: \"%s\"\n", nickname);
			send_connexion_response(c_sock, YES);
			recv_message(c_sock, nickname);
		}
	}
	else if(buffer[0] == 'R'){
		fprintf(stderr, "[DEBUG] READER CONNEXION\n");
		send_connexion_response(c_sock, YES);
		send_message_reader(c_sock);
	}
	else{
		fprintf(stderr, "[DEBUG] MISUNDERSTANDING REQUEST\n");
		send_connexion_response(c_sock, NO);
	}
	free(point);
}

int choose_port(){
	srand(time(NULL)); 
	return PORT_MIN+rand()%(PORT_MAX-PORT_MIN+1);
}

void show_ip(){
	char tmp[256];
	struct addrinfo *info;

	if (gethostname(tmp, sizeof tmp) != 0)
		exit(EXIT_FAILURE);

	if (getaddrinfo(tmp, NULL, NULL, &info) != 0)
		exit(EXIT_FAILURE);

	struct sockaddr_in *p = (struct sockaddr_in *)info->ai_addr;
	inet_ntop(AF_INET, &p->sin_addr, tmp, sizeof tmp);
	puts(tmp);

	freeaddrinfo(info);
}
void send_connexion_response(int c_sock, int yes_no){
	if(yes_no == YES){
		if(send(c_sock, "OK", SIZE_RESP, 0) <= 0){
			perror("send()");
			exit(EXIT_FAILURE);
		}
	}
	else{
		if(send(c_sock, "NO", SIZE_RESP, 0) <= 0){
			perror("send()");
			exit(EXIT_FAILURE);
		}
	}
}

void recv_message(int c_sock, char *nickname){
	char buffer[SIZE_MAXI] = {'\0'};
	char tmp[SIZE_MAXI+20] = {'\0'};
	char date[20];
	int test;
	time_t t = time(NULL);
	struct tm * p;
	while(1){
		p = localtime(&t);
		strftime(date, 20, "%H:%M:%S", p);

		test = recv(c_sock, buffer, sizeof(buffer), 0);
		if(test < 0){
			perror("recv()");
			exit(errno);
		}
		if(test == 0){
			fprintf(stderr, "[DEBUG] Client %s disconnected\n", nickname);
			writer_number --;
			free_writers(nickname);
			break;
		}
		fprintf(stdout, "%s %s\n", date, buffer);
		pthread_mutex_lock(&mutex);
		sprintf(shared_buffer, "%s %s\n", date, buffer);
		pthread_mutex_unlock(&mutex);
		shared_buffer_update = 1;
	}
}
int test_nickname(char *nickname){
	int i;
	for(i = 0; i < 20; i++){
		if(strcmp(writers[i], nickname) == 0){
			fprintf(stderr, "[DEBUG] Nickname is already use\n");
			return -1;
		}
	}
	return 1;
}

int check_writers(){
	int i = 0;
	while(i < 20){
		if(writers[i][0] == '\0')
			return i;
		i ++;
	}
	return -1;
}

void send_message_reader(int c_sock){
	while(1){
		if(shared_buffer_update == 1){
			pthread_mutex_lock(&mutex);
			if(send(c_sock, shared_buffer, strlen(shared_buffer), 0) < 0){
				perror("send()");
				exit(errno);
			}
			pthread_mutex_unlock(&mutex);
			shared_buffer_update = 0;
		}
	}
}

int free_writers(char *nickname){

}

void add_writer(int i, char writers[20][NICKNAME_MAXI], char writer[NICKNAME_MAXI]){
	int j;
	for(j = 0; j < NICKNAME_MAXI; j++){
		writers[i][j] = writer[j];
	}
}

