#ifndef SERVER
#define SERVER

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <netdb.h>
#include <pthread.h>
#include "parser.h"
#include "constant.h"

int choose_port();
void server(struct option_s *options_p);
void * distribution_work(void * point);
void send_connexion_response(int c_sock, int yes_no);
void show_ip();
void recv_message(int c_sock, char *nickname);
int test_nickname(char *nickname);
void send_message_reader(int c_sock);
int check_writers();
void add_writer(int i, char writers[20][NICKNAME_MAXI], char writer[NICKNAME_MAXI]);


#endif
