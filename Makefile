.PHONY: clean, mrproper

CC=gcc
CFLAGS=-g
LDFLAGS=-pthread
OBJ=main.o parser.o server.o client_writer.o client_reader.o
EXEC=main 

all: $(EXEC)

main: main.o parser.o server.o client_writer.o client_reader.o

clean:
	rm -rf *.o
mrproper: clean
	rm -rf $(EXEC)
