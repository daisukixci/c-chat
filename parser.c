#include "parser.h"

void option_parser(int argc, char* argv[], struct option_s *options_p){
	int sFlag = 0, wFlag = 0, rFlag = 0;
	char *ipValue = NULL, *portValue = NULL, *nickValue = NULL ;
	int optionChar;

	while((optionChar = getopt( argc, argv, "swri:p:n:")) != -1 ){
		switch(optionChar){
			case 's':
				sFlag = 1;
				break;
			case 'w':
				wFlag = 1;
				break;
			case 'i':
				ipValue = optarg;
				break;
			case 'p':
				portValue = optarg;
				break;
			case 'n':
				nickValue = optarg;
				break;
			case 'r':
				rFlag = 1;
				break;
			case '?':
				if(optionChar =='i')
					fprintf(stderr, "Option -%c requires arguments\n", optopt);
				else if(optionChar =='p')
					fprintf(stderr, "Option -%c requires arguments\n", optopt);
				else if(optionChar =='n')
					fprintf(stderr, "Option -%c requires arguments\n", optopt);
				else if(isprint(optopt))
					fprintf(stderr, "Unknown option -%c\n", optopt);
				else
					fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
			default:
				break;
		}
	}
	fprintf(stderr, "[DEBUG] OPTION_PARSER: sFlag = %d, wFlag = %d, ipValue = %s, portValue = %s, nickValue = %s, rFlag = %d\n", sFlag, wFlag, ipValue, portValue, nickValue, rFlag);
	if(validation_options(sFlag, wFlag, rFlag) == 0)
		assign_value_options(options_p, sFlag, wFlag, rFlag, ipValue, portValue, nickValue);
	else
		fprintf(stderr, "[DEBUG] ERROR: You choose too many options\n");
}

void assign_value_options(struct option_s* options_p, int sFlag, int wFlag, int rFlag, char *ipValue, char *portValue, char *nickValue){
	options_p->sFlag = sFlag;
	options_p->wFlag = wFlag;
	options_p->rFlag = rFlag;
	if(sFlag != 1){
		options_p->ipValue = ipValue;
		options_p->portValue = atoi(portValue);
		if(wFlag == 1)
			options_p->nickValue = nickValue;
	}
}

void initialize_struct(struct option_s* options_p){
	options_p->sFlag = 0;
	options_p->wFlag = 0;
	options_p->rFlag = 0;
	options_p->ipValue = NULL;
	options_p->portValue = 0;
	options_p->nickValue = NULL;
	fprintf(stderr, "[DEBUG] INITIALIZE_STRUCT: Success\n");
}

int validation_options(int sFlag, int wFlag, int rFlag){
	fprintf(stderr, "[DEBUG] sFlag: %d, wFlag: %d, rFlag: %d\n", sFlag, wFlag, rFlag);
	if(sFlag + rFlag + wFlag == 1 )
		return 0;
	else
		return -1;
} 

int which_service(struct option_s *options_p){
	if(options_p->sFlag == 1){
		if(options_p->ipValue == NULL && options_p->portValue == 0 && options_p->nickValue == NULL )
			return 1;
		else
			return -1;
	}
	else if(options_p->wFlag == 1){
		if(options_p->ipValue != NULL && options_p->portValue != 0 && options_p->nickValue != NULL )
			return 2;
		else
			return -1;
	}
	else if(options_p->rFlag == 1){
		if(options_p->ipValue != NULL && options_p->portValue != 0 && options_p->nickValue == NULL )
			return 3;
		else
			return -1;
	}
	else{
		return -1;
	}
}

char* catch_nickname(char buffer[NICKNAME_MAXI]){
	char *token;

	token = strtok(buffer, " ");
	token = strtok(NULL, " ");

	return token;
}
