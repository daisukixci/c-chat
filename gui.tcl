#! /usr/local/bin wish -f
wm title . "gChat"
set width 1000
set height 500
set connected 0
wm geometry . ${width}x$height

set ip address-ip
set port port
set pseudo pseudo

frame .connexion
grid .connexion -column 1
entry .connexion.i -textvar ::ip -width 15
grid .connexion.i -column 0 -row 0
entry .connexion.p -textvar ::port -width 5
grid .connexion.p -column 0 -row 1 
entry .connexion.n -textvar ::pseudo -width 10
grid .connexion.p -column 0 -row 2 
button .connexion.c -text "Connect" -command {connect $ip $port $pseudo}
grid .connexion.c -column 0 -row 3 
text .t -width 100 -height 25 -state disabled
grid .t -column 0 -row 0 
entry .w -width 100
grid .w -column 0 -row 1
button .s -text "Send" -command {send}
grid .s -column 0 -row 2

proc test { } {
	tk_messageBox -message "test"
}


proc connect { ip port pseudo } {
	exec ./main -w -i ip -p port -n nickname
	set connected 1
	exec ./main -r -i ip -p port 
}

proc send { } {

}
