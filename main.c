#include "parser.h"
#include "client_writer.h"
#include "client_reader.h"
#include "server.h"

int main(int argc, char **argv){
	struct option_s options;
	struct option_s *options_p = &options;

 	initialize_struct(options_p);
 	option_parser(argc, argv, options_p);
 	fprintf(stderr, "[DEBUG] MAIN: sFlag = %d, wFlag = %d, ipValue = %s, portValue = %d, nickValue = %s, rFlag = %d\n", options.sFlag, options.wFlag, options.ipValue, options.portValue, options.nickValue, options.rFlag);
	switch(which_service(options_p)){
		case 1:
			server(options_p);
			break;
		case 2:
			writer_client(options_p);
			break;
		case 3:
			reader_client(options_p);
			break;
		case -1:
			fprintf(stdout, "Invalid Argument\nUsage:\nServer: main -s\nWriter: main -w -i [IP] -p [PORT] -n [NICKNAME]\nReader: -r -i [IP] -p [PORT]\n");
			break;
	}

	return EXIT_SUCCESS;
}
