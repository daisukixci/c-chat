#include "client_writer.h"

void writer_client(struct option_s *options_p){
	struct sockaddr_in socket_in = {0};
	int sock = socket( AF_INET, SOCK_STREAM, 0);
	int chat_quit = 1;
	if(sock == INVALID_SOCKET){
		perror("socket()");
		exit(errno);
	}
	inet_aton(options_p->ipValue, &socket_in.sin_addr);
	socket_in.sin_port = htons(options_p->portValue);
	socket_in.sin_family = AF_INET;
	if(connect(sock, (struct sockaddr *)&socket_in, sizeof(struct sockaddr)) == SOCKET_ERROR){
		perror("connect()");
		exit(errno);
	}
	send_nickname(options_p->nickValue, sock);
	if(writer_connexion_response(sock, options_p) == NICKNAME_OK){
		while (chat_quit) {
			chat_quit = send_message(sock, chat_quit, options_p);
		}
		close(sock);
	}

}

void backSlashNTracking(char* target, int size){
	int i;
	for(i=0;i<size;i++){
		if(target[i]=='\n'){
			target[i]='\0';
		}
	}
}

int send_message(int sock, int chat_quit, struct option_s *options_p){
	char buffer[SIZE_MAXI] = {'\0'};
	char tmp[SIZE_MAXI] = {'\0'};
	fgets(buffer, SIZE_MAXI, stdin);
	backSlashNTracking(buffer, SIZE_MAXI);
	quit_test(buffer, chat_quit);
	sprintf(tmp, "%s: %s", options_p->nickValue, buffer);
	fprintf(stderr, "[DEBUG] You: %s\n", buffer);
	if(send(sock, tmp, strlen(tmp), 0) < 0){
			perror("send()");
	 		exit(errno);
	}
	return chat_quit;
}

void send_nickname(char* nickname, int sock){
	char tmp[NICKNAME_MAXI];
	sprintf (tmp, "W %s", nickname);
	fprintf(stderr, "[DEBUG] tmp: %s\n", tmp);
	if(send(sock, tmp, strlen(tmp), 0) < 0){
 		perror("send()");
 		exit(errno);
 	}
}

int writer_connexion_response(int sock, struct option_s* option_p){
	char buffer[SIZE_RESP];
	if(recv(sock, buffer, sizeof buffer, 0) > 0){
		if(strcmp(buffer, "OK") == 0){
			fprintf(stderr, "[DEBUG]: CONNEXION RESPONSE OK\n");
			fprintf(stderr, "[DEBUG]: nickname: %s is accepted\n", option_p->nickValue);
			return NICKNAME_OK;
		}
		else if(strcmp(buffer, "NO") == 0){
			fprintf(stderr, "[DEBUG]: CONNEXION RESPONSE NO\n");
			fprintf(stderr, "[DEBUG]: nickname is already use: %s \n", option_p->nickValue);
			return NICKNAME_NO;
		}
		else{
			fprintf(stderr, "[DEBUG]: CONNEXION RESPONSE NO RECOGNIZE: %s ?\n", buffer);
		}
	}
	else{
		fprintf(stderr, "[DEBUG]: Error in response for nickname ask\n");
		return ERROR;
	}
}

int quit_test(char * buffer, int chat_quit){
	if(strcmp(buffer, ":q\0") == 0){
		chat_quit = 0;
	}
	return chat_quit;
}
