#include "client_reader.h"

void reader_client(struct option_s *options_p){
	struct sockaddr_in socket_in = {0};
	int sock = socket( AF_INET, SOCK_STREAM, 0);
	int chat_quit = 1;
	int read_flag = 1;
	if(sock == INVALID_SOCKET){
		perror("socket()");
		exit(errno);
	}
	inet_aton(options_p->ipValue, &socket_in.sin_addr);
	socket_in.sin_port = htons(options_p->portValue);
	socket_in.sin_family = AF_INET;
	if(connect(sock, (struct sockaddr *)&socket_in, sizeof(struct sockaddr)) == SOCKET_ERROR){
		perror("connect()");
		exit(errno);
	}
	send_read_request(sock);
	if(reader_connexion_response(sock) == READER_OK){
		receive_message(sock);
		close(sock);
	}

}

void receive_message(int sock){
	char buffer[SIZE_MAXI] = {'\0'};
	while(1){
		if(recv(sock, buffer, sizeof(buffer), 0) <= 0){
			perror("recv()");
			exit(errno);
		}
		fprintf(stdout, "%s\n", buffer);
	}
}

int send_read_request(int sock){
	char reader_flag[2] = "R";
	if(send(sock, reader_flag, strlen(reader_flag), 0) <= 0){
		perror("send()");
		exit(errno);
	}
}

int reader_connexion_response(int sock){
	char buffer[SIZE_RESP] = {'0'};
	if(recv(sock, buffer, sizeof(buffer), 0) > 0){
		if(strcmp(buffer, "OK") == 0){
			fprintf(stderr, "[DEBUG]: CONNEXION RESPONSE OK\n");
			return READER_OK;
		}
		else if(strcmp(buffer, "NO") == 0){
			fprintf(stderr, "[DEBUG]: CONNEXION RESPONSE NO\n");
			fprintf(stderr, "[DEBUG]: TOO CONNEXION RETRYLATER\n");
			return READER_NO;
		}
		else{
			fprintf(stderr, "[DEBUG]: CONNEXION RESPONSE NO RECOGNIZE\n");

		}
	}
	else{
		fprintf(stderr, "[DEBUG]: Error in response for connexion ask");
		return ERROR;
	}
}	
