#ifndef PARSER
#define PARSER


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include "constant.h"

struct option_s{
	int sFlag;
	int wFlag;
	int rFlag;
	char *ipValue;
	int portValue;
	char *nickValue;
};
	
void option_parser(int argc, char* argv[], struct option_s *options);
void initialize_struct(struct option_s *options);
void assign_value_options(struct option_s* options, int sFlag, int wFlag, int rFlag, char *ipValue, char *portValue, char *nickValue);
int validation_options(int sFlag, int wFlag, int rFlag);
int which_service(struct option_s *options_p);
//char* catch_nickname(char *buffer);
char* catch_nickname(char buffer[NICKNAME_MAXI]);

#endif
