#ifndef CONSTANT 
#define CONSTANT

#define SIZE_MAXI 1024
#define NICKNAME_MAXI 50
#define SIZE_HOST 50
#define SIZE_RESP 10

#define PORT_MIN 1024
#define PORT_MAX 65535

#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define NICKNAME_OK 1
#define NICKNAME_NO -1
#define ERROR -1
#define READER_OK 1
#define READER_NO -1
#define SOCKET_ERROR -1
#define INVALID_SERVER_SOCKET -1
#define YES 1
#define NO -1

#endif
