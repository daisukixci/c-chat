#ifndef CLIENT_READER
#define CLIENT_READER

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include "parser.h"
#include "constant.h"


void reader_client(struct option_s *options_p);
void receive_message(int socket);
int send_read_request(int sock);
int reader_connexion_response(int sock);

#endif
