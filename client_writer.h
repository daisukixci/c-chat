#ifndef CLIENT_WRITER
#define CLIENT_WRITER

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include "parser.h"
#include "constant.h"


void writer_client(struct option_s *options_p);
int quit_test(char * buffer, int chat_quit);
int send_message(int sock, int chat_quit, struct option_s *options_p);
void send_nickname(char* nickname, int socket);
void backSlashNTracking(char* target, int size);
int writer_connexion_response(int sock, struct option_s* option_p);

#endif
